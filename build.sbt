//import AssemblyKeys._ // put this at the top of the file

import com.banno.license.Plugin.LicenseKeys._

import com.banno.license.Licenses._

com.github.retronym.SbtOneJar.oneJarSettings

//assemblySettings

licenseSettings

license := """Copyright (c) 2014 T.Munkhdalai
This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
"""

//removeExistingHeaderBlock := true

name := "BioEE-preprocess"

version := "1.0"

scalaVersion := "2.10.3"

libraryDependencies += "com.google.guava" % "guava" % "15.0"

libraryDependencies += "com.google.code.findbugs" % "jsr305" % "1.3.+"

libraryDependencies += "edu.arizona.sista" % "processors" % "2.0"

libraryDependencies += "edu.arizona.sista" % "processors" % "2.0" classifier "models"

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging-slf4j" % "2.1.2"

libraryDependencies += "com.typesafe" % "config" % "1.2.1"

libraryDependencies += "org.scalanlp" %% "breeze" % "0.8.1"

libraryDependencies += "org.scalanlp" %% "breeze-natives" % "0.8.1"