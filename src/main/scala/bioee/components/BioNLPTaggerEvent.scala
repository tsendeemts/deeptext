/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.components

import bioee.extraction.BioNLPEventConstructorComponent
import bioee.extraction.BioNLPRelationExtractionComponent
import bioee.dataset.generation.BioNLPExampleBuilderComponent
import bioee.text.preprocessing.BioNLPPreprocessingComponent
import bioee.utils.BioNLPConstants
import bioee.utils.StemmedDictionary
import bioee.utils.Conf
import bioee.dataset.generation.BioNLPDatasetBuilderComponent
import bioee.dataset.generation.BioNLPRelationBuilderComponent
import bioee.extraction.BioNLPEventConstructorFromEventComponent
import bioee.extraction.BioNLPEventExtractionComponent

trait BioNLPTaggerEvent extends BioNLPEventConstructorFromEventComponent with BioNLPEventExtractionComponent
  with BioNLPExampleBuilderComponent with BioNLPPreprocessingComponent {

  val conf = new Conf("/home/tsendeemts/pros/deeptext/src/main/resources/application-event-prod.conf")

  val WVPath = conf.root.getString("wv.path")
  val wvLcase = conf.root.getBoolean("wv.lcase")
  val wvLetterNgram = conf.root.getBoolean("wv.letterNgram")
  val wvNonNumeric = conf.root.getBoolean("wv.nonnumeric")
  val indexVectorSize = conf.root.getInt("features.ivector.size")
  val positionVectorPath = conf.root.getString("features.ivector.path")
  val skipAbbrevs = conf.root.getBoolean("preprocessing.loading.skipAbbrevs")
  val dictPathTrigger = conf.root.getString("preprocessing.loading.dic.trigger")

  val a1fileKey: String = BioNLPConstants.a1fileKey
  val a2fileKey: String = BioNLPConstants.a2fileKey
  val argRolesToProcess = BioNLPConstants.roles

  val tokenizer = new SimpleTokenizer
  val mentionTaggger = new BioNLPGeniaMentionLoader(a1fileKey, a2fileKey, skipAbbrevs)
  val relationLoader = new BioNLPGeniaRelationLoader(a2fileKey, argRolesToProcess, false)
  val sentenceSplitter = new CoreNLPSentenceSplitter

  val dic = new StemmedDictionary(dictPathTrigger)
  val exampleBuilder = new BioNLPEventExampleBuilder(WVPath, 100, wvLcase, wvNonNumeric, wvLetterNgram)

  val tagExampleOut = conf.root.getString("tag.dev.example")
  val tagA2Out = conf.root.getString("tag.dev.a2")

  val relationExtractor = new BioNLPGeniaEventExtractor(dic)
  val relationAnnotater = Map(
    BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI ->
      new BioNLPGeniaEventAnnotater(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI, tagExampleOut,
        conf.root.getString("simple.prot-tri.crf"), conf.root.getString("simple.prot-tri.map")),
    BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT ->
      new BioNLPGeniaEventAnnotater(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT, tagExampleOut,
        conf.root.getString("simple.prot-tri-prot.crf"), conf.root.getString("simple.prot-tri-prot.map")),
    BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI ->
      new BioNLPGeniaEventAnnotater(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI, tagExampleOut,
        conf.root.getString("complex.event-tri.crf"), conf.root.getString("complex.event-tri.map")),
    BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT ->
      new BioNLPGeniaEventAnnotater(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT, tagExampleOut,
        conf.root.getString("complex.event-tri-prot.crf"), conf.root.getString("complex.event-tri-prot.map")))
  val eventConstructor = new BioNLPEventConstructorFromEvent(tagA2Out)

}