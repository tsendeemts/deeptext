/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.extraction

import bioee.dataset.generation.ExampleBuilderComponent
import bioee.domains._
import bioee.text.preprocessing.PreprocessingComponent
import bioee.utils.Dictionary
import bioee.utils._
import bioee.utils.BioNLPUtils._
import java.io.File
import bioee.utils.RichFile._
import scala.sys.process._

trait BioNLPRelationExtractionComponent extends ExtractionComponent {
  this: ExampleBuilderComponent with PreprocessingComponent =>

  class BioNLPGeniaRelationExtractor(triDic: Dictionary) extends RelationExtractor {

    def extract(doc: Document): Document = {
      tokenizer.annotate(doc)
      sentenceSplitter.annotate(doc)
      mentionTaggger.annotate(doc)
      var rId = 0
      doc.sentences.foreach { sent =>
        val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
        val triggerCandidates = sent.tokens.filter(token => triDic.exists(token.word))
        var argMentionChildIdsTheme = protMentions.map(m => (m, None, None))
        generateNoneRelation(argMentionChildIdsTheme, triggerCandidates, BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI,
          BioNLPConstants.ThemeNone, sameRelationFilterRuleToken(Seq(), sent))
          .foreach { relation =>
            relation.eventId = Some(rId.toString)
            relation.relationId = rId.toString
            sent.createRelation(relation)
            doc.createRelation(relation)
            rId = rId + 1
          }
      }
      exampleBuilder.build(doc)
      relationAnnotater(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI).extract(doc)
      doc.sentences.foreach { sent =>
        val relations = sent.relations
        val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
        val compEvn = relations.filter(r => BioNLPConstants.isComplex(r.relationType.predictedValue.get))
        val triMentionChildIdsCause = compEvn.map(r => (r.centerMention, None, r.eventId))
        generateNoneRelationProt(triMentionChildIdsCause, protMentions, BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT,
          BioNLPConstants.CauseNone, sameRelationFilterRuleToken(Seq(), sent))
          .foreach { relation =>
            relation.relationId = rId.toString
            sent.createRelation(relation)
            doc.createRelation(relation)
            rId = rId + 1
          }
      }
      exampleBuilder.build(doc)
      relationAnnotater(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI_PROT).extract(doc)
      doc.sentences.foreach { sent =>
        val relations = sent.relations
        val bindingRelations = relations.filter(relation => relation.relationType.predictedValue.getOrElse("") == BioNLPConstants.Binding)
          .toIndexedSeq
        for (i <- 0 until bindingRelations.length; j <- i + 1 until bindingRelations.length) {
          val arg1 = bindingRelations(i).argumentMention
          val arg2 = bindingRelations(j).argumentMention
          val relation = Relation(relationId = rId.toString,
            relationType = Label(), centerMention = arg1,
            argumentMention = arg2, argumentRole = Label(),
            groupId = Set(BioNLPConstants.RelationGroups.BINDING_PAIR), childId = None)
          sent.createRelation(relation)
          doc.createRelation(relation)
          rId = rId + 1
        }
      }
      exampleBuilder.build(doc)
      relationAnnotater(BioNLPConstants.RelationGroups.BINDING_PAIR).extract(doc)
      doc
    }
  }

  class BioNLPGeniaRelationExtractorCompact(triDic: Dictionary) extends RelationExtractor {

    def extract(doc: Document): Document = {
      tokenizer.annotate(doc)
      sentenceSplitter.annotate(doc)
      mentionTaggger.annotate(doc)
      var rId = 0
      doc.sentences.foreach { sent =>
        val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
        val triggerCandidates = sent.tokens.filter(token => triDic.exists(token.word))
        val argMentionChildIdsTheme = protMentions.map(m => (m, None, None))
        generateNoneRelation(argMentionChildIdsTheme, triggerCandidates, BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI,
          BioNLPConstants.ThemeNone, sameRelationFilterRuleToken(Seq(), sent))
          .foreach { relation =>
            relation.eventId = Some(rId.toString)
            relation.relationId = rId.toString
            sent.createRelation(relation)
            doc.createRelation(relation)
            rId = rId + 1
          }
      }
      exampleBuilder.build(doc)
      relationAnnotater(BioNLPConstants.RelationGroups.SIMPLE_EVENT_PROT_TRI).extract(doc)
      doc.sentences.foreach { sent =>
        val relations = sent.relations
        val triggerCandidates = sent.tokens.filter(token => triDic.exists(token.word))
        val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
        val argMentionChildIdsTheme = protMentions.map(m => (m, None, None))
        generateNoneRelation(argMentionChildIdsTheme, triggerCandidates, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI,
          BioNLPConstants.ThemeNone, sameRelationFilterRuleToken(Seq(), sent))
          .foreach { relation =>
            relation.eventId = Some(rId.toString)
            relation.relationId = rId.toString
            sent.createRelation(relation)
            doc.createRelation(relation)
            rId = rId + 1
          }
        val triggersSimple = relations.filter(r => BioNLPConstants.isSimpleWithBinding(r.relationType.predictedValue.get))
          .map(r => (r.centerMention, None, r.eventId))
        generateNoneRelation(triggersSimple, triggerCandidates, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI,
          BioNLPConstants.ThemeNone, sameRelationFilterRuleToken(Seq(), sent))
          .foreach { relation =>
            relation.relationId = rId.toString
            sent.createRelation(relation)
            doc.createRelation(relation)
            rId = rId + 1
          }
      }
      exampleBuilder.build(doc)
      relationAnnotater(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI).extract(doc)
      doc.sentences.foreach { sent =>
        val relations = sent.relations
        val protMentions = sent.getMentionByTrueLabel(BioNLPConstants.Protein)
        val rSimple = relations.filter(r => BioNLPConstants.isSimpleWithBinding(r.relationType.predictedValue.get))
        val rComplex = relations.filter(r => BioNLPConstants.isComplex(r.relationType.predictedValue.get))
        val triggersComplex = rComplex.map(r => (r.centerMention, None, r.eventId))
        generateNoneRelationProt(triggersComplex, protMentions, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT,
          BioNLPConstants.CauseNone, sameRelationFilterRuleToken(Seq(), sent))
          .foreach { relation =>
            relation.relationId = rId.toString
            sent.createRelation(relation)
            doc.createRelation(relation)
            rId = rId + 1
          }
        val triggersSimple = rSimple.map(r => r.centerMention)
        generateNoneRelationProt(triggersComplex, triggersSimple, BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT,
          BioNLPConstants.CauseNone, sameRelationFilterRuleToken(Seq(), sent))
          .foreach { relation =>
            relation.relationId = rId.toString
            sent.createRelation(relation)
            doc.createRelation(relation)
            rId = rId + 1
          }
      }
      exampleBuilder.build(doc)
      relationAnnotater(BioNLPConstants.RelationGroups.COMPLEX_REG_EVENT_TRI_PROT).extract(doc)
      doc.sentences.foreach { sent =>
        val relations = sent.relations
        val bindingRelations = relations.filter(relation => relation.relationType.predictedValue.getOrElse("") == BioNLPConstants.Binding)
          .toIndexedSeq
        for (i <- 0 until bindingRelations.length; j <- i + 1 until bindingRelations.length) {
          val arg1 = bindingRelations(i).argumentMention
          val arg2 = bindingRelations(j).argumentMention
          val relation = Relation(relationId = rId.toString,
            relationType = Label(), centerMention = arg1,
            argumentMention = arg2, argumentRole = Label(),
            groupId = Set(BioNLPConstants.RelationGroups.BINDING_PAIR), childId = None)
          sent.createRelation(relation)
          doc.createRelation(relation)
          rId = rId + 1
        }
      }
      exampleBuilder.build(doc)
      relationAnnotater(BioNLPConstants.RelationGroups.BINDING_PAIR).extract(doc)
      doc
    }
  }

  class BioNLPGeniaRelationAnnotater(rGroup: String, dataDir: String, classifierPath: String, indexAndLabel: String) extends RelationAnnotater {
    val index2Label = indexAndLabel.split("\t").map { l =>
      val ls = l.split(" -> ")
      (ls(1).trim.toInt, ls(0).trim)
    }.toMap

    def extract(doc: Document): Document = {
      val relations = doc.relations.filter(_.groupId(rGroup))
      val docDir = new File(dataDir + doc.docId)
      val exampleList = new File(dataDir + doc.docId + "/example.list." + rGroup)
      if (!docDir.exists()) docDir.mkdir()
      val paths = relations.map { relation =>
        val example = relation.example.get
        val path = dataDir + doc.docId + "/" + relation.relationId + "." + rGroup + ".0"
        example.save(path = path, docId = doc.docId, r = relation)
        path
      }.toIterator
      exampleList.lines = paths
      s"th $classifierPath $dataDir/${doc.docId}".!!
      val labels = new File(dataDir + doc.docId + "/label.list." + rGroup).lines.filter(_ != "").map { l =>
        index2Label(l.toInt)
      }.toIndexedSeq
      relations.zipWithIndex.foreach { pair =>
        val (relation, i) = pair
        val lbls = labels(i).split(":")
        relation.relationType = Label(predictedValue = Some(lbls(0)))
        relation.argumentRole = Label(predictedValue = Some(lbls(1)))
      }
      doc
    }
  }

}