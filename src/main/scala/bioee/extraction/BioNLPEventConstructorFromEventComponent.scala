/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.extraction

import bioee.domains._
import bioee.utils.BioNLPConstants
import bioee.utils.BioNLPUtils
import java.io.File
import bioee.utils.RichFile._

trait BioNLPEventConstructorFromEventComponent extends EventConstructorComponent {
  this: ExtractionComponent =>

  class BioNLPEventConstructorFromEvent(outputDir: String) extends EventConstructor {
    val outDir = new File(outputDir)

    def construct(doc: Document): Document = {
      relationExtractor.extract(doc)
      var mention2tId: Map[String, String] = Map()
      var tId = doc.mentions.size

      def mentionId(k: String) = {
        mention2tId.getOrElse(k, {
          tId = tId + 1
          mention2tId = mention2tId + (k -> tId.toString)
          tId.toString
        })
      }
      var eId = 0
      doc.sentences.foreach { sent =>
        sent.events.foreach { event =>
          //          !event.id.startsWith("E")
          event.id = s"E$eId"
          eId += 1
          val tri = event.trigger
          val triId = mentionId(event.eventType.predictedValue.get + tri.head.indexInDocument)
          tri.mentionId = Some(triId)
        }
      }
      doc
    }

    def constructFromDir(dataSourceDir: String, maxDocs: Int = Int.MaxValue) = {
      val dataDir = new File(dataSourceDir)
      val docs = BioNLPUtils.loadDocs(dir = dataDir, maxCount = maxDocs, loadA2File = false)
      docs.foreach { doc =>
        val a2file = new File(outDir, doc.docId + ".a2")
        construct(doc)
        val events = doc.events
        val triStr = events.map { e =>
          e.trigger.mentionId.get -> s"T${e.trigger.mentionId.get}\t${e.eventType.predictedValue.get} ${e.trigger.head.start} ${e.trigger.head.end}\t${e.trigger.head.word}"
        }.toMap.map(_._2)
        val eStr = events.map { event =>
          var themStr = if (event.childTheme.isEmpty) {
            val themArg = event.arguments(0)
            s"${BioNLPConstants.Theme}:${themArg.argument.mentionId.get}"
          } else {
            s"${BioNLPConstants.Theme}:${event.childTheme.get.id}"
          }
          val eventType = event.eventType.predictedValue.get
          themStr = if (eventType == BioNLPConstants.Binding || eventType == BioNLPConstants.SameBinding) {
            if (event.arguments.size > 1) {
              val them2 = event.arguments(1).argument.mentionId.get
              s"$themStr ${BioNLPConstants.Theme}2:$them2"
            } else {
              themStr
            }
          } else {
            if (event.childCause.isEmpty) {
              if (event.arguments.size > 1) {
                val causeArg = event.arguments(1)
                s"$themStr ${BioNLPConstants.Cause}:${causeArg.argument.mentionId.get}"
              } else {
                themStr
              }
            } else {
              s"$themStr ${BioNLPConstants.Cause}:${event.childCause.get.id}"
            }
          }
          s"${event.id}\t${event.eventType.predictedValue.get}:T${event.trigger.mentionId.get} $themStr"
        }
        a2file.lines = (triStr ++ eStr).toIterator
      }
    }

  }

}