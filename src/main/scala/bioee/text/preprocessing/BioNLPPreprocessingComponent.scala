/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.text.preprocessing

import bioee.domains._
import bioee.text.segmentation.tokenization.{ SimpleTokenizer => jSimpleTokenizer }
import scala.collection.JavaConversions._
import edu.arizona.sista.processors.Processor
import edu.arizona.sista.processors.corenlp.CoreNLPProcessor
import java.io.File
import com.typesafe.scalalogging.slf4j.LazyLogging
import scala.collection.mutable.ArrayBuffer
import bioee.utils.BioNLPUtils
import bioee.utils.BioNLPConstants
import sun.util.logging.resources.logging

trait BioNLPPreprocessingComponent extends PreprocessingComponent {

  class SimpleTokenizer extends Tokenizer {
    val simpleTokenizer = new jSimpleTokenizer

    def annotate(doc: Document) = {
      val tokenSeq = simpleTokenizer.tokenize(doc.text).toIndexedSeq
      val tokens = tokenSeq.zipWithIndex
        .map { token =>
          Token(start = token._1.start, end = token._1.end, word = token._1.word, indexInDocument = token._2)
        }
      doc.tokens = tokens
      doc
    }

    def annotate(sentence: Sentence) = ???

  }

  class CoreNLPSentenceSplitter extends SentenceSplitter {
    val coreNLPProcessor = new CoreNLPProcessor()

    def annotate(doc: Document) = {
      val sentences = coreNLPProcessor.mkDocument(doc.text).sentences.zipWithIndex
        .map { sentenceId =>
          val sent = sentenceId._1
          val indexIndDocument = sentenceId._2
          val start = sent.startOffsets(0)
          val end = sent.endOffsets(sent.endOffsets.length - 1)
          val text = sent.getSentenceText
          val sen = Sentence(id = indexIndDocument.toString, start = start, end = end, text = text, indexInDocument = indexIndDocument)
          sen.fillTokens(doc)
          sen
        }
      doc.sentences = sentences
      doc
    }

    def annotate(sentence: Sentence) = ???

  }

  class BioNLPGeniaMentionLoader(a1fileKey: String, a2fileKey: String, skipAbbrevs: Boolean) extends MentionLoader with LazyLogging {
    var mentionLabels: Set[String] = Set()

    def annotate(doc: Document) = {
      doc.tagSourceFile.get(a1fileKey).map { sourcePath =>
        val a1file = new File(sourcePath)
        annotateEntityMention(a1file, doc)
        logger.info("Added mentions from a1 file " + a1file.getAbsolutePath)
      }
      doc.tagSourceFile.get(a2fileKey).map { sourcePath =>
        val a2file = new File(sourcePath)
        annotateTriggerMention(a2file, doc)
        logger.info("Added mentions from a2 file " + a2file.getAbsolutePath)
      }
      doc
    }

    def annotate(sentence: Sentence) = ???

    def annotateEntityMention(a1file: File, doc: Document) = {
      import bioee.utils.RichFile
      import bioee.utils.RichFile.enrichFile

      val added = new ArrayBuffer[Mention]
      for (line <- a1file.lines) {
        val Array(id, labelAndOffsets, text) = line.split("\t")
        val Array(label, begin, end) = labelAndOffsets.split(" ")
        mentionLabels = mentionLabels + label
        for (token <- doc.tokenAt(end.toInt - 1)) {
          val beginInt = if (text.startsWith(" ")) begin.toInt + 1 else begin.toInt
          val tokenAt = doc.tokenAt(beginInt)
          if (tokenAt == None) {
            error("No token can be found for %s at %d in doc %s".format(text, begin.toInt, doc.tokens.map(_.word).mkString(" ")))
          }
          val beginToken = tokenAt.get
          val sentenceAt = doc.sentenceAt(beginToken)
          //We treat first token of mention as head (center)
          if (!skipAbbrevs || !added.lastOption.map(last => last.end.indexInDocument == beginToken.indexInDocument - 2
            && doc.tokens.get(beginToken.indexInDocument - 1).word == "(").getOrElse(false)) {
            val mentionLabel = Label(trueValue = Option(label))
            val mention = Mention(mentionId = Option(id), mentionLabel = mentionLabel, head = beginToken, begin = beginToken,
              end = token)
            sentenceAt.get.createMention(mention)
            doc.createMention(mention)
            added += mention
          } else {
            logger.info("Skipped entity %s due to abbreviation".format(id))
          }
        }
      }
      doc
    }

    def annotateTriggerMention(a2file: File, doc: Document) = {
      import bioee.utils.RichFile
      import bioee.utils.RichFile.enrichFile

      val added = new ArrayBuffer[Mention]
      for (line <- a2file.lines if (line.startsWith("T"))) {
        val Array(id, labelAndOffsets, text) = line.split("\t")
        val Array(label, begin, end) = labelAndOffsets.split(" ")
        val labelNorm = if (BioNLPConstants.Entity == label) {
          BioNLPConstants.Protein
        } else {
          label
        }
        mentionLabels = mentionLabels + labelNorm
        for (token <- doc.tokenAt(end.toInt - 1)) {
          val beginInt = if (text.startsWith(" ")) begin.toInt + 1 else begin.toInt
          val tokenAt = doc.tokenAt(beginInt)

          if (tokenAt == None) {
            error("No token can be found for %s at %d in doc %s".format(text, begin.toInt, doc.tokens.map(_.word).mkString(" ")))
          }
          val beginToken = tokenAt.get
          val sentenceAt = doc.sentenceAt(beginToken)
          //We treat first token of mention as head (center)
          if (!skipAbbrevs || !added.lastOption.map(last => last.end.indexInDocument == beginToken.indexInDocument - 2
            && doc.tokens.get(beginToken.indexInDocument - 1).word == "(").getOrElse(false)) {
            val mentionLabel = Label(trueValue = Option(labelNorm))
            val mention = Mention(mentionId = Option(id), mentionLabel = mentionLabel, head = beginToken, begin = beginToken,
              end = token)
            sentenceAt.get.createMention(mention)
            doc.createMention(mention)
            added += mention
          } else {
            logger.info("Skipped entity %s due to abbreviation".format(id))
          }
        }

      }
      doc
    }
  }

  class BioNLPGeniaRelationLoader(a2fileKey: String, argRolesToProcess: Set[String], roleNorm: Boolean = true) extends RelationLoader with LazyLogging {
    import bioee.utils.RichFile
    import bioee.utils.RichFile.enrichFile

    def annotate(doc: Document) = {
      doc.tagSourceFile.get(a2fileKey).map { sourcePath =>
        val a2file = new File(sourcePath)
        val id2typeAndTrigger = a2file.lines.filter(_.startsWith("E")).map { line =>
          val Array(id, typeAndTrigger, rest @ _*) = line.split("\\s+")
          (id -> (typeAndTrigger, rest))
        }.toMap
        for ((id, (typeAndTrigger, rest)) <- id2typeAndTrigger) {
          val Array(relationType, triggerId) = typeAndTrigger.split(":")
          assert(!doc.mentions.isEmpty, s"Doc should at least contain a mention or be mention-tagged: ${doc.docId}")
          val centerMention = doc.getMentionById(triggerId)
          if (!centerMention.isEmpty) {
            rest.foreach { arg =>
              val Array(role, argId) = arg.split(":")
              val normRole = if (roleNorm) BioNLPUtils.normalizeRole(role) else role
              /**
               * We ignore arg roles of task 2, for now
               * Take only arg roles of task 1: Theme, Cause, Theme2
               */
              if (argRolesToProcess(normRole)) {
                val mayArgumentMention = if (argId.startsWith("E")) {
                  val (typeAndTrigger, rest) = id2typeAndTrigger.get(argId).get
                  val Array(relationType, triggerId) = typeAndTrigger.split(":")
                  val mayArgumentMention = doc.getMentionById(triggerId)
                  if (mayArgumentMention.isEmpty) {
                    val mayArgumentMention = doc.getMentionById("T" + (triggerId.replace("T", "").toInt - 1))
                    logger.trace(s"Loading long name: ${mayArgumentMention.get} of abbvr: $triggerId")
                    mayArgumentMention
                  } else {
                    mayArgumentMention
                  }
                } else {
                  val mayArgumentMention = doc.getMentionById(argId)
                  if (mayArgumentMention.isEmpty) {
                    val mayArgumentMention = doc.getMentionById("T" + (argId.replace("T", "").toInt - 1))
                    logger.trace(s"Loading long name: ${mayArgumentMention.get} of abbvr: $argId")
                    mayArgumentMention
                  } else {
                    mayArgumentMention
                  }
                }
                assert(!mayArgumentMention.isEmpty, s"'$argId' not found in doc mention: \n ${doc.mentions.map(m => (m.mentionId, m.head.word))}")
                val argumentMention = mayArgumentMention.get
                val (groupId, childEventId) = if (argumentMention.mentionLabel.trueValue.get == BioNLPConstants.Protein) {
                  (BioNLPConstants.RelationGroups.SIMPLE_EVENT, None)
                } else {
                  (BioNLPConstants.RelationGroups.COMPLEX_REG, Some(argId))
                }
                val relationTypeLabel = Label(trueValue = Option(relationType))
                val roleLabel = Label(trueValue = Option(normRole))
                val relation = Relation(eventId = Some(id), relationId = s"$id:$normRole", relationType = relationTypeLabel,
                  centerMention = centerMention.get, argumentMention = argumentMention,
                  argumentRole = roleLabel, groupId = Set(groupId), childId = childEventId)
                doc.sentenceAt(centerMention.get.head).get.createRelation(relation)
                doc.createRelation(relation)
              }
            }
          } else {
            logger.error(s"Center mention with id: ${triggerId} not found in doc: ${doc.docId}")
          }
        }
      }
      doc
    }

    def annotate(sentence: Sentence) = ???
  }

}