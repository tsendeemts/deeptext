/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.domains

import java.io.PrintWriter

case class Sentence(id: String, start: Int, end: Int,
  text: String, var tokens: Seq[Token] = null,
  indexInDocument: Int) extends StructuredText {

  def fillTokens(doc: Document) = {
    val startToken = doc.tokens.filter(token => token.start <= start && token.end >= start)
    val middle = doc.tokens.filter(token => token.start > start && token.end < end)
    val endToken = doc.tokens.filter(token => token.start <= end && token.end >= end)
    tokens = (startToken ++ middle ++ endToken)
      .zipWithIndex.map { token =>
        token._1.indexInSentence = token._2
        token._1
      }
    tokens
  }

  override def toString() = {
    tokens.map(_.word).mkString(" ")
  }

  def saveExample(path: String, separator: String = ",") = {
    val out = new PrintWriter(path, "UTF-8")
    try {
      example.foreach { ex =>
        val features = ex.features.get
        for (j <- 0 until features.cols) {
          val colVector = features(::, j)
          val colString = colVector.toArray.toList.mkString(separator)
          out.println(colString)
        }
      }
    } finally {
      out.close
    }
  }

}