/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.domains

import java.io.File
import com.typesafe.scalalogging.slf4j.LazyLogging
import java.io.PrintWriter

case class Dataset(var datasetId: String, var description: String,
  var examples: Seq[Example], var labelIndex: Map[String, Int] = Map(),
  datasetDirPath: String, negativeLabel: String, addNewLabel: Boolean,
  var classDistr: Map[String, Int] = Map()) extends LazyLogging {

  val datasetDir = new File(datasetDirPath)
  if (!datasetDir.exists()) {
    logger.trace(s"Creating dir: ${datasetDir.getAbsolutePath()}")
    datasetDir.mkdir()
  }

  def label2Index(label: String) = {
    classDistr = classDistr + (label -> (classDistr.getOrElse(label, 0) + 1))
    labelIndex.getOrElse(label, {
      if (addNewLabel) {
        val nindex = labelIndex.size
        labelIndex = labelIndex + (label -> nindex)
        nindex
      } else {
        labelIndex.get(negativeLabel).get
      }
    })
  }

  override def toString() = {
    description + "\ntotal examples: " + classDistr.foldLeft(0)(_ + _._2) + "\n" + classDistr.mkString("\n") + "\n" +
      labelIndex.mkString("\t")
  }

  def save(path: String) = {
    val out = new PrintWriter(path, "UTF-8")
    try {
      out.println(toString)
    } finally {
      out.close
    }
  }

}