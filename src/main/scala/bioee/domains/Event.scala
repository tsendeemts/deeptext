/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.domains

case class Event(var id: String, var eventType: Label,
  var trigger: Mention, var arguments: Seq[RoleMention],
  var relations: Seq[Relation], childTheme: Option[Event] = None,
  childCause: Option[Event] = None, var exampleTheme: Option[Example] = None,
  var exampleCause: Option[Example] = None,
  var groupId: Set[String] = Set()) {

  def toSeqTheme() = {
    val themSeq = if (childTheme.isEmpty) {
      Seq(arguments(0).argument)
    } else {
      childTheme.get.toSeq
    }
    val triSeq = Seq(trigger)
    themSeq ++ triSeq
  }

  /**
   *  TODO: check events are cyclic
   */
  def toSeq(): Seq[Mention] = {
    val themSeq = if (childTheme.isEmpty) {
      Seq(arguments(0).argument)
    } else {
      childTheme.get.toSeq()
    }
    val triSeq = Seq(trigger)
    val causSeq = if (childCause.isEmpty) {
      if (arguments.length > 1) {
        Seq(arguments(1).argument)
      } else {
        Seq()
      }
    } else {
      childCause.get.toSeq()
    }
    themSeq ++ triSeq ++ causSeq
  }
}

case class RoleMention(var role: Label, var argument: Mention)