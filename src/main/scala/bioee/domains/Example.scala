/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.domains

import breeze.linalg.DenseMatrix
import java.io.File
import java.io.PrintWriter
import bioee.utils.BioNLPConstants

case class Example(exampleId: String,
  features: Option[DenseMatrix[Float]] = None, var label: Label = Label(), var labelIndex: Option[Int] = None) {

  override def toString() = {
    exampleId + "  " +
      label.trueValue.getOrElse("None")
  }

  def save(path: String, separator: String = ",", docId: String, r: Relation) = {
    val out = new PrintWriter(path, "UTF-8")
    try {
      out.println(s"--${toString()}\t${r.centerMention.head.word.trim} ${r.argumentMention.head.word.trim}")
      out.println(s"--${label.trueValue.get}")
      out.println(s"--${labelIndex.getOrElse(0)}")
      out.println(s"id:${docId}:${r.eventId.getOrElse(BioNLPConstants.None)}")
      out.println(s"childid:${docId}:${r.childId.getOrElse(BioNLPConstants.None)}")
      out.print(s"\n")
      for (j <- 0 until features.get.cols) {
        val colVector = features.get(::, j)
        val colString = colVector.toArray.toList.mkString(separator)
        out.println(colString)
      }
    } finally {
      out.close
    }
  }
  
  def saveEvent(path: String, separator: String = ",", docId: String, e: Event, sentPath: String) = {
    val out = new PrintWriter(path, "UTF-8")
    try {
      out.println(s"--${toString()}\t${e.trigger.head.word.trim}")
      out.println(s"--${label.trueValue.get}")
      out.println(s"--${labelIndex.getOrElse(0)}")
      out.println(s"sent:${sentPath}")
      out.println(s"id:${docId}:${e.id}")
      out.print(s"\n")
      for (j <- 0 until features.get.cols) {
        val colVector = features.get(::, j)
        val colString = colVector.toArray.toList.mkString(separator)
        out.println(colString)
      }
    } finally {
      out.close
    }
  }
}