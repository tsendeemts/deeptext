/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.dataset.generation

import bioee.text.preprocessing.PreprocessingComponent
import bioee.domains._
import bioee.utils.BioNLPConstants
import breeze.linalg._
import bioee.utils.Word2Vec
import java.io.File
import bioee.utils.BioNLPUtils
import com.typesafe.scalalogging.slf4j.LazyLogging
import bioee.utils.TextHelpers
import bioee.utils.RichFile
import bioee.utils.RichFile.enrichFile
import java.io.PrintWriter
import bioee.utils.Dictionary
import bioee.utils.BioNLPUtils._
import bioee.dataset.generation.BioNLPExampleBuilderComponent.BioNLPGeniaExampleBuilder
import bioee.utils.Word2Index
import bioee.utils.Suffix2Index
import bioee.utils.BrownCluster
import bioee.utils.BrownPath2Index

trait BioNLPExampleBuilderComponent extends ExampleBuilderComponent {

  class BioNLPGeniaExampleBuilderWithPositionVector(WVPath: String, positionVectorSize: Int, positionVectorPath: String,
    wvLcase: Boolean, wvNonNumeric: Boolean, wvLetterNgram: Boolean)
    extends BioNLPGeniaExampleBuilder(WVPath: String, wvLcase: Boolean, wvNonNumeric: Boolean, wvLetterNgram: Boolean)
    with ExampleBuilder {

    var position2vector: Map[Int, DenseVector[Float]] = {
      val file = new File(positionVectorPath)
      if (file.exists()) {
        file.lines.map { line =>
          val ds = line.split("\\s+")
          ds(0).toInt -> DenseVector(ds.tail.map(_.toFloat))
        }.toMap
      } else {
        Map()
      }
    }

    def getPositionVector(position: Int) = {
      position2vector.getOrElse(position, {
        val wv = DenseVector.rand(positionVectorSize).map(_.toFloat)
        position2vector = position2vector + (position -> wv)
        wv
      })
    }

    def savePositionVector() = {
      val out = new PrintWriter(positionVectorPath, "UTF-8")
      try {
        position2vector.foreach { vec =>
          out.println(vec._1 + " " + vec._2.toArray.toList.mkString(" "))
        }
      } finally {
        out.close
      }
    }

    def build(doc: Document) = {
      doc.sentences.foreach { sent =>
        val tokens = sent.tokens
        val WVMatrix = buildWV(tokens)
        sent.relations.foreach { relation =>
          if (relation.example.isEmpty) {
            val features = DenseMatrix.zeros[Float](WVSize + positionVectorSize * 2, tokens.size)
            for (i <- 0 until tokens.size) {
              val center2TokenPosition = tokens(i).indexInSentence -
                relation.centerMention.head.indexInSentence
              val arg2TokenPosition = tokens(i).indexInSentence -
                relation.argumentMention.head.indexInSentence
              features(::, i) := DenseVector.vertcat(
                WVMatrix(::, i),
                getPositionVector(center2TokenPosition),
                getPositionVector(arg2TokenPosition))
            }
            val exampleLabel = if (!relation.argumentRole.trueValue.isEmpty) {
              relation.relationType.trueValue.get + ":" + relation.argumentRole.trueValue.get
            } else {
              relation.relationType.trueValue.get
            }
            val example = Example(exampleId = relation.relationId,
              features = Option(features), label = Label(trueValue = Option(exampleLabel)))
            relation.example = Some(example)
          }
        }
      }
      doc
    }

    def build(dataSourceDir: String, maxDocs: Int) = ???

  }

  class BioNLPGeniaExampleBuilderWithPositionIndex(WVPath: String, maxIndex: Int = 100,
    wvLcase: Boolean, wvNonNumeric: Boolean, wvLetterNgram: Boolean,
    WVLearn: Boolean = false, WVVocPath: String = "", minFreq: Int = 5,
    useOtherFeatures: Boolean = false, suffixPath: String = "",
    brownModelPath: String = "", brownPath2IndexPath: String = "")
    extends BioNLPGeniaExampleBuilder(WVPath: String, wvLcase: Boolean, wvNonNumeric: Boolean, wvLetterNgram: Boolean,
      WVLearn: Boolean, WVVocPath: String, minFreq: Int,
      useOtherFeatures: Boolean, suffixPath: String,
      brownModelPath: String, brownPath2IndexPath: String)
    with ExampleBuilder {

    def build(doc: Document) = {
      doc.sentences.foreach { sent =>
        val tokens = sent.tokens
        val WVMatrix = buildWV(tokens)
        sent.relations.foreach { relation =>
          /**
           * Last two values in each column define the relative position between center mention and token
           * and argument mention and token, respectively
           */
          if (relation.example.isEmpty) {
            val features = DenseMatrix.zeros[Float](nFeatures + 2, tokens.size)
            for (i <- 0 until tokens.size) {
              val center2TokenPosition = normIndex(tokens(i).indexInSentence -
                relation.centerMention.head.indexInSentence, maxIndex)
              val arg2TokenPosition = normIndex(tokens(i).indexInSentence -
                relation.argumentMention.head.indexInSentence, maxIndex)
              features(::, i) := DenseVector.vertcat(
                WVMatrix(::, i),
                DenseVector(center2TokenPosition.toFloat),
                DenseVector(arg2TokenPosition.toFloat))
            }
            val exampleLabel = if (!relation.argumentRole.trueValue.isEmpty) {
              relation.relationType.trueValue.get + ":" + relation.argumentRole.trueValue.get
            } else {
              relation.relationType.trueValue.getOrElse(BioNLPConstants.None)
            }
            val example = Example(exampleId = relation.relationId,
              features = Option(features), label = Label(trueValue = Option(exampleLabel)))
            relation.example = Some(example)
          }
        }
      }
      doc
    }

    def build(dataSourceDir: String, maxDocs: Int) = ???

  }

  class BioNLPEventExampleBuilder(WVPath: String, maxIndex: Int = 100,
    wvLcase: Boolean, wvNonNumeric: Boolean, wvLetterNgram: Boolean,
    WVLearn: Boolean = false, WVVocPath: String = "", minFreq: Int = 5)
    extends BioNLPGeniaExampleBuilder(WVPath: String, wvLcase: Boolean, wvNonNumeric: Boolean, wvLetterNgram: Boolean,
      WVLearn: Boolean, WVVocPath: String, minFreq: Int)
    with ExampleBuilder {

    def build(doc: Document) = {
      doc.sentences.foreach { sent =>
        val tokens = sent.tokens
        val WVMatrix = buildWV(tokens)
        val example = Example(exampleId = sent.id, features = Option(WVMatrix))
        sent.example = Some(example)
        sent.events.foreach { e =>
          if (e.exampleTheme.isEmpty) {
            val seqThem = e.toSeqTheme.toArray
            e.exampleTheme = Some(toExample(seqThem, sent, e))
          }
          if (e.exampleCause.isEmpty) {
            val seqCaus = e.toSeq.toArray
            e.exampleCause = Some(toExample(seqCaus, sent, e))
          }
        }
      }
      doc
    }

    def toExample(seqMens: Array[Mention], sent: Sentence, e: Event) = {
      val tokens = sent.tokens.toArray
      val features = DenseMatrix.zeros[Float](seqMens.length, 1)
      for (i <- 0 until seqMens.length) {
        features(i, 0) = seqMens(i).head.indexInSentence
      }
      val lbl = e.eventType.trueValue.getOrElse(BioNLPConstants.None)
      Example(exampleId = e.id,
        features = Option(features), label = Label(trueValue = Option(lbl)))
    }
  }
}

object BioNLPExampleBuilderComponent {

  abstract class BioNLPGeniaExampleBuilder(WVPath: String, wvLcase: Boolean,
    wvNonNumeric: Boolean, wvLetterNgram: Boolean,
    WVLearn: Boolean = false, WVVocPath: String = "", minFreq: Int = 5,
    useOtherFeatures: Boolean = false, suffixPath: String = "",
    brownModelPath: String = "", brownPath2IndexPath: String = "") extends LazyLogging {
    val WVs = new Word2Vec
    val WVVoc = new Word2Index
    if (WVLearn) {
      WVVoc.loadVoc(WVVocPath, minFreq)
    } else {
      WVs.loadText(WVPath)
    }
    val suffix2Index = new Suffix2Index(suffixPath)
    val brownModel = new BrownCluster(brownModelPath)
    val brownPath2Index = new BrownPath2Index(brownPath2IndexPath)

    val WVSize = WVs.vectorSize
    val unknowWV = DenseVector.zeros[Float](WVSize)

    val nFeatures = {
      var nIndex = if (WVLearn) {
        1
      } else {
        WVSize
      }
      nIndex = if (useOtherFeatures) nIndex + 9 else nIndex
      nIndex
    }

    /**
     * builds WV matrix for given token sequence
     * Processes tokens through text processing pipeline
     */
    def buildWV(tokens: Seq[Token]) = {
      /**
       * d x token.size, column is a WV for a token
       * d is WV size
       */
      val WVMatrix = DenseMatrix.zeros[Float](nFeatures, tokens.size)
      implicit def bool2int(b: Boolean) = if (b) 1 else 0

      for (i <- 0 until tokens.size) {
        val wordRaw = tokens(i).word.trim()
        val word = normToken(wordRaw)
        var wv = if (WVLearn) {
          val i = WVVoc.toIndex(word)
          DenseVector(Array[Float](i))
        } else {
          val WV = WVs.vector(word)
          if (WV.length == 0) {
            logger.warn(s"WV for a word is not found: $word ")
            unknowWV
          } else {
            DenseVector(WV)
          }
        }
        wv = if (useOtherFeatures) {
          DenseVector.vertcat(wv,
            DenseVector(Array[Float](suffix2Index.toIndex(wordRaw),
              brownPath2Index.toIndex(brownModel.wordPrefix(wordRaw)),
              TextHelpers.allAlpha(wordRaw).toInt,
              TextHelpers.allCaps(wordRaw).toInt,
              TextHelpers.hasDigit(wordRaw).toInt,
              TextHelpers.initCaps(wordRaw).toInt,
              TextHelpers.isPunct(wordRaw).toInt,
              TextHelpers.lowerToUpper(wordRaw).toInt,
              TextHelpers.upperToLower(wordRaw).toInt)))
        } else wv
        WVMatrix(::, i) := wv
      }
      WVMatrix
    }

    def normToken(token: String) = {
      var word = if (wvLcase) {
        token.toLowerCase()
      } else token
      word = if (wvNonNumeric) {
        TextHelpers.normNumeric(word)
      } else word
      word
    }

    def normIndex(i: Int, maxIndex: Int) = {
      val ai = i + maxIndex
      if (ai < 0) 0 else if (ai > maxIndex) maxIndex else ai
    }

  }

}