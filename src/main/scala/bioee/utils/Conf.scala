/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.utils

import com.typesafe.config._
import java.io.File

class Conf(conf: String) {

  val myCfg = ConfigFactory.parseFile(new File(conf))
  val rootConfig = ConfigFactory.load(myCfg)
  println(rootConfig)
  
  def root = rootConfig
}