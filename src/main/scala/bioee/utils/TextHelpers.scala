/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.utils

import com.google.common.base.CharMatcher

object TextHelpers {

  val NUMERIC_PLACE_HOLDER = "*NUM*"
  val UNKNOWN = "*UNKNOWN*"
  val UNKNOWN_SUFFIX = "#UNKNOWN#"
  val NUMERIC_SUFFIX = "#NUM#"
  val UNKNOWN_BROWN_PREFIX = "#UNKNOWN#"

  def replaceSpaces(str: String, rep: Char = ' ') = CharMatcher.WHITESPACE.trimAndCollapseFrom(str, rep);

  def isNumeric(word: String) = {
    if (word == NUMERIC_PLACE_HOLDER) {
      true
    } else {
      CharMatcher.anyOf(",.-").removeFrom(word)
        .matches("-?\\d+(\\.\\d+)?")
    }

  }

  def normNumeric(token: String) = {
    if (TextHelpers.isNumeric(token)) {
      TextHelpers.NUMERIC_PLACE_HOLDER
    } else {
      token
    }
  }

  def letterNgram(token: String, ngram: Int = 3) = {
    if (TextHelpers.isNumeric(token)) {
      IndexedSeq(token)
    } else {
      val word = "#" + token + "#"
      val letterNgrams = for (i <- 0 until (word.length() - ngram + 1)) yield {
        word.substring(i, i + ngram)
      }
      letterNgrams
    }
  }

  def nSuffix(token: String, nSuffix: Int = 3) = {
    if (TextHelpers.isNumeric(token)) {
      NUMERIC_SUFFIX
    } else {
      if (token.length() >= nSuffix) {
        token.substring(token.length - nSuffix, token.length)
      } else {
        token
      }
    }
  }

  def allCaps(token: String) = token.matches("[A-Z]+")

  def allAlpha(t: String) = t.matches("[A-Za-z]+")

  def initCaps(t: String) = t.matches("[A-Z].*")

  def upperToLower(t: String) = t.matches("[A-Z][a-z].*")

  def lowerToUpper(t: String) = t.matches("[a-z]+[A-Z]+.*")

  def hasDigit(t: String) = t.matches(".*[0-9].*")

  def isPunct(t: String) = t.matches("[`~!@#$%^&*()-=_+\\[\\]\\\\{}|;\':\\\",./<>?]+")
}