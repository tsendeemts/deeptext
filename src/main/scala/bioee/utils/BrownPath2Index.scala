/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.utils

import java.io.File
import bioee.utils.RichFile.enrichFile

class BrownPath2Index(voc: String, minFreq: Int = 20) {
  
  // 992 > 20
  lazy val s2i: Map[String, Int] = {
    val w2f = new File(voc)
    w2f.lines.toList.map(l => l.split("\t")).filter(_(1).toInt > minFreq).map(_(0)).zipWithIndex.toMap
  }

  def toIndex(path: String) = {
    s2i.getOrElse(path, s2i(TextHelpers.UNKNOWN))
  }

}