/*
 * Copyright (c) 2014 T.Munkhdalai
 * This file is part of the DeepText event extraction system, https://bitbucket.org/tsendeemts/deeptext
 * This software is provided under the terms of the Common Public License, version 1.0, as published by http://www.opensource.org.  For further information, see the file 'LICENSE.txt' included with this distribution.
 */
package bioee.utils

import bioee.utils.RichFile.enrichFile
import java.io.File
import bioee.text.segmentation.tokenization.{ SimpleTokenizer => jSimpleTokenizer }
import org.tartarus.snowball.ext.englishStemmer
import scala.collection.JavaConversions._

trait Dictionary {
  def exists(token: String): Boolean
}

class StemmedDictionary(dicPath: String) extends Dictionary {
  val simpleTokenizer = new jSimpleTokenizer
  val stemmer = new englishStemmer

  val stemSet = new File(dicPath).lines.flatMap { line =>
    val tokens = simpleTokenizer.tokenize(line.toLowerCase()).map { token =>
      stem(token.word)
    }
    tokens
  }.toSet

  def exists(token: String) = {
    val stems = stem(token.toLowerCase())
    stemSet(stems)
  }

  def stem(token: String) = {
    stemmer.setCurrent(token)
    stemmer.stem()
    stemmer.getCurrent()
  }
}

class AllAcceptDictionary extends Dictionary {
  def exists(token: String) = true
}